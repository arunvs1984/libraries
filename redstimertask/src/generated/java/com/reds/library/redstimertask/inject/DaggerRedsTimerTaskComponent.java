package com.reds.library.redstimertask.inject;

import com.reds.library.redstimertask.info.RedsTimerTaskInfo;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerRedsTimerTaskComponent implements RedsTimerTaskComponent {
  private Provider<RedsTimerTaskInfo> provideRedsTimerTaskInfoProvider;

  private DaggerRedsTimerTaskComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static RedsTimerTaskComponent create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.provideRedsTimerTaskInfoProvider =
        DoubleCheck.provider(
            RedsTimerTaskModule_ProvideRedsTimerTaskInfoFactory.create(
                builder.redsTimerTaskModule));
  }

  @Override
  public RedsTimerTaskInfo takeRedsTimerTaskInfo() {
    return provideRedsTimerTaskInfoProvider.get();
  }

  public static final class Builder {
    private RedsTimerTaskModule redsTimerTaskModule;

    private Builder() {}

    public RedsTimerTaskComponent build() {
      if (redsTimerTaskModule == null) {
        this.redsTimerTaskModule = new RedsTimerTaskModule();
      }
      return new DaggerRedsTimerTaskComponent(this);
    }

    public Builder redsTimerTaskModule(RedsTimerTaskModule redsTimerTaskModule) {
      this.redsTimerTaskModule = Preconditions.checkNotNull(redsTimerTaskModule);
      return this;
    }
  }
}
