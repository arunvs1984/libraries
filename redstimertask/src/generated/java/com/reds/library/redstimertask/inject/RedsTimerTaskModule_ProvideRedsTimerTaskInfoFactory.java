package com.reds.library.redstimertask.inject;

import com.reds.library.redstimertask.info.RedsTimerTaskInfo;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RedsTimerTaskModule_ProvideRedsTimerTaskInfoFactory
    implements Factory<RedsTimerTaskInfo> {
  private final RedsTimerTaskModule module;

  public RedsTimerTaskModule_ProvideRedsTimerTaskInfoFactory(RedsTimerTaskModule module) {
    this.module = module;
  }

  @Override
  public RedsTimerTaskInfo get() {
    return provideInstance(module);
  }

  public static RedsTimerTaskInfo provideInstance(RedsTimerTaskModule module) {
    return proxyProvideRedsTimerTaskInfo(module);
  }

  public static RedsTimerTaskModule_ProvideRedsTimerTaskInfoFactory create(
      RedsTimerTaskModule module) {
    return new RedsTimerTaskModule_ProvideRedsTimerTaskInfoFactory(module);
  }

  public static RedsTimerTaskInfo proxyProvideRedsTimerTaskInfo(RedsTimerTaskModule instance) {
    return Preconditions.checkNotNull(
        instance.provideRedsTimerTaskInfo(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
