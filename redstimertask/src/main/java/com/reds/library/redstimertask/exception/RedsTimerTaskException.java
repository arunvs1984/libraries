package com.reds.library.redstimertask.exception;
import com.reds.library.redscommons.exception.AbstractPlatformException;

/**
 * <b>Purpose:</b> Exception class for RedsTimerTask
 *
 * @author <b>SHINEED BASHEER</b>
 *
 *			This code is generated by Reds
 */
public class RedsTimerTaskException extends AbstractPlatformException {

	/**
	 * Default serialVersionId
	 */
	private static final long serialVersionUID = 1L;

	public RedsTimerTaskException(String code) {
		super(code);
	}

	/**
	 * @param code - Error Code
	 * @param message
	 */
	public RedsTimerTaskException(String code, String message) {
		super(code, message);

	}

	/**
	 * @param code  - Error Code
	 * @param message
	 * @param cause
	 */
	public RedsTimerTaskException(String code, String message, Throwable cause) {
		super(code, message, cause);

	}

	/**
	 * @param code  - Error Code
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedsTimerTaskException(String code, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(code, message, cause, enableSuppression, writableStackTrace);

	}

}
