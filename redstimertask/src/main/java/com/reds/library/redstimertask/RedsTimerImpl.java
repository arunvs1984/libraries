package com.reds.library.redstimertask;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.reds.library.redstimertask.exception.RedsTimerTaskErrorCodes;
import com.reds.library.redstimertask.exception.RedsTimerTaskException;

public class RedsTimerImpl<V> implements RedsTimer<V> {

	private ScheduledExecutorService executorService = null;

	private Map<String, ScheduledFuture<V>> registry;

	@Override
	public void init(int threadCount) {
		this.executorService = Executors.newScheduledThreadPool(threadCount);
		this.registry = new HashMap<String, ScheduledFuture<V>>();

	}

	@Override
	public void scheduleTask(String key, RedsAbstractTimerTask<V> timerTask, long delay, TimeUnit time)
			throws RedsTimerTaskException {
		if (delay <= 0) {
			throw new RedsTimerTaskException(RedsTimerTaskErrorCodes.errorCode(10),
					"Invalid Delay Configured. Delay always greater than Zero");
		}
		this.registry.put(key, this.executorService.schedule((Callable<V>) timerTask, delay, time));

	}

	@Override
	public void scheduleFixedDelayTask(String key, RedsAbstractTimerTask<V> timerTask, long initialDelay, long delay,
			TimeUnit unit) throws RedsTimerTaskException {
		if (delay <= 0) {
			throw new RedsTimerTaskException(RedsTimerTaskErrorCodes.errorCode(20),
					"Invalid Delay Configured. Delay always greater than Zero");
		}
		@SuppressWarnings("unchecked")
		ScheduledFuture<V> temp = (ScheduledFuture<V>) this.executorService.scheduleWithFixedDelay(timerTask,
				initialDelay, delay, unit);
		this.registry.put(key, temp);

	}

	@Override
	public void cancelTask(String key) {
		if (this.registry.get(key) != null) {
			ScheduledFuture<V> scheduledFuture = registry.get(key);
			scheduledFuture.cancel(true);
			registry.remove(key);
		}

	}

	@Override
	public void cancelAllTasks() {
		for (String key : registry.keySet()) {
			cancelTask(key);
		}

	}

	@Override
	public long getRemainingTime(String key, TimeUnit timeUnit) {

		ScheduledFuture<V> scheduledFuture = registry.get(key);
		long remainingTime = 0;
		if (null != scheduledFuture) {
			remainingTime = scheduledFuture.getDelay(timeUnit);
		}
		return remainingTime;
	}

}
