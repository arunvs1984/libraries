package com.reds.library.redstimertask;

import java.util.concurrent.Callable;

import com.reds.library.redstimertask.exception.RedsTimerTaskErrorCodes;
import com.reds.library.redstimertask.exception.RedsTimerTaskException;
import com.reds.library.redstimertask.exception.RedsTimerTaskRuntimeException;
import com.reds.library.redstimertask.tracker.RedsTimerTaskTrack;

public abstract class RedsAbstractTimerTask<V> implements Callable<V>, Runnable {

	public abstract V doThis() throws RedsTimerTaskException;

	public RedsAbstractTimerTask() {

	}

	@Override
	public V call() throws Exception {

		return doThis();
	}

	@Override
	public void run() {
		try {
			doThis();
		} catch (Exception e) {
			RedsTimerTaskTrack.me.error("Failed to execute task ",e);
		}
	}

}
