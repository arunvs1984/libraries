package com.reds.library.redstimertask;

import java.util.concurrent.TimeUnit;

import com.reds.library.redstimertask.exception.RedsTimerTaskException;

public interface RedsTimer<V> {

	public void init(int threadCount);

	public void scheduleTask(String key, RedsAbstractTimerTask<V> timerTask, long delay, TimeUnit time) throws RedsTimerTaskException;

	public void scheduleFixedDelayTask(String key,RedsAbstractTimerTask<V> timerTask, long initialDelay, long delay, TimeUnit time) throws RedsTimerTaskException;

	public void cancelTask(String key);

	public void cancelAllTasks();

	public long getRemainingTime(String key, TimeUnit timeUnit);
}
