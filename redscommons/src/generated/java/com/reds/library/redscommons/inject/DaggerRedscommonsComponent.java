package com.reds.library.redscommons.inject;

import com.reds.library.redscommons.info.RedscommonsInfo;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerRedscommonsComponent implements RedscommonsComponent {
  private RedscommonsModule redscommonsModule;

  private DaggerRedscommonsComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static RedscommonsComponent create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.redscommonsModule = builder.redscommonsModule;
  }

  @Override
  public RedscommonsInfo takeRedscommonsInfo() {
    return RedscommonsModule_ProvideRedscommonsInfoFactory.proxyProvideRedscommonsInfo(
        redscommonsModule);
  }

  public static final class Builder {
    private RedscommonsModule redscommonsModule;

    private Builder() {}

    public RedscommonsComponent build() {
      if (redscommonsModule == null) {
        this.redscommonsModule = new RedscommonsModule();
      }
      return new DaggerRedscommonsComponent(this);
    }

    public Builder redscommonsModule(RedscommonsModule redscommonsModule) {
      this.redscommonsModule = Preconditions.checkNotNull(redscommonsModule);
      return this;
    }
  }
}
