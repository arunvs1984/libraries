package com.reds.library.redscommons.inject;

import com.reds.library.redscommons.info.RedscommonsInfo;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RedscommonsModule_ProvideRedscommonsInfoFactory
    implements Factory<RedscommonsInfo> {
  private final RedscommonsModule module;

  public RedscommonsModule_ProvideRedscommonsInfoFactory(RedscommonsModule module) {
    this.module = module;
  }

  @Override
  public RedscommonsInfo get() {
    return provideInstance(module);
  }

  public static RedscommonsInfo provideInstance(RedscommonsModule module) {
    return proxyProvideRedscommonsInfo(module);
  }

  public static RedscommonsModule_ProvideRedscommonsInfoFactory create(RedscommonsModule module) {
    return new RedscommonsModule_ProvideRedscommonsInfoFactory(module);
  }

  public static RedscommonsInfo proxyProvideRedscommonsInfo(RedscommonsModule instance) {
    return Preconditions.checkNotNull(
        instance.provideRedscommonsInfo(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
