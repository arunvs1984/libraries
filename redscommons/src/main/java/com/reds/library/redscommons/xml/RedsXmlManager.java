package com.reds.library.redscommons.xml;

import java.util.List;

import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.to.PlatformTO;

/**
 * <b>Purpose:</b> Interface class for all XML managers
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 * @param <T>
 */
public interface RedsXmlManager<T extends PlatformTO> {

	/**
	 * <b>Purpose:</b> To write {@link PlatformTO} to disk as xml
	 * 
	 * @param redsDo
	 * @throws RedsXmlException
	 */
	public void write(T redsDo) throws RedsXmlException;

	/**
	 * <b>Purpose:</b> To read {@link PlatformTO} from disk.
	 * 
	 * @param redsDo
	 * @param clazz
	 * @return
	 * @throws RedsXmlException
	 */
	public T read(T redsDo, Class<?> clazz) throws RedsXmlException;
	
	/**
	 * <b>Purpose:</b> To read {@link PlatformTO} from disk.
	 * 
	 * @param location
	 * @param clazz
	 * @return
	 * @throws RedsXmlException
	 */
	public T read(String location, Class<?> clazz) throws RedsXmlException;

	/**
	 * <b>Purpose:</b> To delete {@link PlatformTO}
	 * 
	 * @param redsDo
	 * @throws RedsXmlException
	 */
	public void delete(T redsDo) throws RedsXmlException;

	/**
	 * <b>Purpose:</b> To delete all {@link PlatformTO}
	 * 
	 * 
	 * @throws RedsXmlException
	 */
	public void deleteAll() throws RedsXmlException;

	/**
	 * <b>Purpose:</b> To read all {@link PlatformTO}
	 * 
	 * @param redsDo
	 * @param clazz
	 * @return
	 * @throws RedsXmlException
	 */
	public List<T> readAll(Class<?> clazz) throws RedsXmlException;

	/**
	 * <b>Purpose:</b> To check if {@link PlatformTO} exist
	 * 
	 * @param redsDo
	 * @return
	 * @throws RedsXmlException
	 */
	public boolean exist(T redsDo) throws RedsXmlException;

}
