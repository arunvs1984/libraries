package com.reds.library.redscommons.config;

import com.reds.library.redscommons.to.PlatformTO;

public interface PlatformConfig extends PlatformTO {

	public String getDescription();

	default public PlatformConfig getDefault() {
		return this;
	}

}
