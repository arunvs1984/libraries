package com.reds.library.redscommons.tracker;

import java.util.Map.Entry;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.api.LayoutComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.LoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;




public class PlatformTrack implements Tracker {

	public static Logger me = LogManager.getLogger(PlatformTrack.class.getName());
	
	private static final String CONSOLE_APPENDER="Stdout";
	private static final String FILE_APPENDER="RollingFile";

	public synchronized static Logger initialize(TrackerConfig trackerConfig) {
		ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
		builder.setStatusLevel(TrackerLevel.getLevel(trackerConfig.getStatusLevel()));
		
		
		
		builder.setConfigurationName("RollingBuilder");
		// create a console appender
		AppenderComponentBuilder appenderBuilder = null;
		if (trackerConfig.isConsoleRequired()) {
			appenderBuilder = builder
					.newAppender(CONSOLE_APPENDER, "CONSOLE")
					.addAttribute("target",	ConsoleAppender.Target.SYSTEM_OUT);
			appenderBuilder.add(builder.newLayout("PatternLayout")
					.addAttribute("pattern", trackerConfig.getPattern()));
			builder.add(appenderBuilder);
		}
		

		// create a rolling file appender
		LayoutComponentBuilder layoutBuilder = builder
				.newLayout("PatternLayout")
				.addAttribute("pattern",trackerConfig.getPattern());
		ComponentBuilder triggeringPolicy = builder.newComponent("Policies")
				
				.addComponent(builder.newComponent("CronTriggeringPolicy")
				.addAttribute("schedule", trackerConfig.getCronTriggeringSchedule()))
				
				.addComponent(builder.newComponent("SizeBasedTriggeringPolicy")
				.addAttribute("size", trackerConfig.getRollBackFileSize()));
		
		triggeringPolicy.build();

		if(trackerConfig.getArchiveFilePattern()!=null&&!trackerConfig.getArchiveFilePattern().isEmpty()) {
			
			appenderBuilder = builder.newAppender(FILE_APPENDER, "RollingFile")
					.addAttribute("fileName", trackerConfig.getFileName())
					.addAttribute("filePattern",trackerConfig.getArchiveFilePattern())
					.add(layoutBuilder)
					.addComponent(triggeringPolicy);
		}
		

		builder.add(appenderBuilder);

		// create the new logger
		LoggerComponentBuilder logBuilder=builder.newLogger(trackerConfig.getName(), TrackerLevel.getLevel(trackerConfig.getLevel()))				
				.add(builder.newAppenderRef(CONSOLE_APPENDER))
				.add(builder.newAppenderRef(FILE_APPENDER))
				.addAttribute("additivity", trackerConfig.isAdditivity());
		setExtras(trackerConfig,logBuilder);
		builder.add(logBuilder);

		//builder.add(builder.newRootLogger(Level.DEBUG)
			   //.add(builder.newAppenderRef("rolling")));
		

		BuiltConfiguration configBuilt = builder.build();
		
		final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		final Configuration config = ctx.getConfiguration();
		config.addLogger(trackerConfig.getName(), 
				configBuilt.getLogger(trackerConfig.getName()));
		configBuilt.getAppender(FILE_APPENDER).start();
		if(trackerConfig.isConsoleRequired()) {
			configBuilt.getAppender(CONSOLE_APPENDER).start();	
		}		
		ctx.updateLoggers();
		//Logger me = LogManager.getLogger(PlatformTrack.class.getName());
		Logger me=LogManager.getLogger(trackerConfig.getName());
		return me;
	}

	
	private static void setExtras(TrackerConfig trackerConfig, LoggerComponentBuilder builder) {
		if(trackerConfig.getExtras()!=null) {
			for(Entry<String, String> entry:trackerConfig.getExtras().entrySet()) {
				builder.addAttribute(entry.getKey(), entry.getValue());		
				
			}
		}
		
	}


	public synchronized static void init(String serviceName) {
		TrackerConfig trackerConfig= new TrackerConfig(serviceName);		
		initialize(trackerConfig);
		
	}
	
	

	public synchronized static Logger getTracker(TrackerConfig trackerConfig) {
		me=initialize(trackerConfig);
		return me;
	}
}
