package com.reds.library.redscommons.tracker;

import java.util.HashMap;
import java.util.Map;

import com.reds.library.redscommons.config.PlatformConfig;

public class TrackerConfig implements PlatformConfig {

	private String id = "TrackerConfig";
	private String name = "platform";
	private TrackerLevel statusLevel = TrackerLevel.WARN;
	private TrackerLevel level = TrackerLevel.DEBUG;
	private String pattern = "%d [%t] %-5level: %msg%n%throwable";
	private String cronTriggeringSchedule = "0 0 0 * * ?";
	private String rollBackFileSize = "100M";
	private String fileName = "./logs/platform.log";
	private String archiveFilePattern = "logs/archive/reds-%d{MM-dd-yy}.log.gz";
	private boolean additivity = false;
	private boolean consoleRequired = true;
	
	private Map<String, String> extras=new HashMap<String, String>();

	public TrackerConfig() {
		

	}

	public TrackerConfig(String name) {
		super();
		this.name = name;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public boolean isConsoleRequired() {
		return consoleRequired;
	}

	public void setConsoleRequired(boolean consoleRequired) {
		this.consoleRequired = consoleRequired;
	}

	public String getCronTriggeringSchedule() {
		return cronTriggeringSchedule;
	}

	public void setCronTriggeringSchedule(String cronTriggeringSchedule) {
		this.cronTriggeringSchedule = cronTriggeringSchedule;
	}

	public String getRollBackFileSize() {
		return rollBackFileSize;
	}

	public void setRollBackFileSize(String rollBackFileSize) {
		this.rollBackFileSize = rollBackFileSize;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getArchiveFilePattern() {
		return archiveFilePattern;
	}

	public void setArchiveFilePattern(String archiveFilePattern) {
		this.archiveFilePattern = archiveFilePattern;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAdditivity() {
		return additivity;
	}

	public void setAdditivity(boolean additivity) {
		this.additivity = additivity;
	}

	public TrackerLevel getStatusLevel() {
		return statusLevel;
	}

	public void setStatusLevel(TrackerLevel statusLevel) {
		this.statusLevel = statusLevel;
	}

	public void setLevel(TrackerLevel level) {
		this.level = level;
	}

	public TrackerLevel getLevel() {
		return level;
	}

	@Override
	public String getId() {

		return this.id;
	}

	@Override
	public String getDescription() {

		return "Tracker Configuration";
	}

	public Map<String, String> getExtras() {
		return extras;
	}

	public void setExtras(Map<String, String> extras) {
		this.extras = extras;
	}

}
