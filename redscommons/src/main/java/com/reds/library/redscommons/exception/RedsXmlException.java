package com.reds.library.redscommons.exception;

public class RedsXmlException extends AbstractPlatformException {
	/**
	 * Default serialVersionId
	 */
	private static final long serialVersionUID = 1L;

	public RedsXmlException(String code) {
		super(code);
	}

	/**
	 * @param code    - Error Code
	 * @param message
	 */
	public RedsXmlException(String code, String message) {
		super(message, code);

	}

	/**
	 * @param code    - Error Code
	 * @param message
	 * @param cause
	 */
	public RedsXmlException(String code, String message, Throwable cause) {
		super(code, message, cause);

	}

	/**
	 * @param code               - Error Code
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedsXmlException(String code, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(code, message, cause, enableSuppression, writableStackTrace);

	}

}
