package com.reds.library.redscommons.tracker;

import org.apache.logging.log4j.Level;

public enum TrackerLevel {
	OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE, ALL;

	public static Level getLevel(TrackerLevel level) {
		Level log4jLevel = Level.DEBUG;
		switch (level) {
		case ALL:
			log4jLevel = Level.ALL;
			break;
		case DEBUG:
			log4jLevel = Level.DEBUG;
			break;
		case ERROR:
			log4jLevel = Level.ERROR;
			break;
		case FATAL:
			log4jLevel = Level.FATAL;
			break;
		case INFO:
			log4jLevel = Level.INFO;
			break;
		case OFF:
			log4jLevel = Level.OFF;
			break;
		case TRACE:
			log4jLevel = Level.TRACE;
			break;
		case WARN:
			log4jLevel = Level.WARN;
			break;
		default:
			log4jLevel = Level.DEBUG;
			break;
		}

		return log4jLevel;
	}

}
