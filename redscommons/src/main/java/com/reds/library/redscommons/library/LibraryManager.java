package com.reds.library.redscommons.library;

import com.reds.library.redscommons.config.PlatformConfig;
import com.reds.library.redscommons.info.Info;

public interface LibraryManager<T extends PlatformConfig> extends AutoCloseable {

	public void init(T config);

	public Info getInfo();

}
