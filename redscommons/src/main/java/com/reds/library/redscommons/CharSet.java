package com.reds.library.redscommons;

public enum CharSet {
	US_ASCII, ISO_8859_1, UTF_8, UTF_16BE, UTF_16LE, UTF_16;

}
