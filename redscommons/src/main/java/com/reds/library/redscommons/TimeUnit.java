package com.reds.library.redscommons;

public enum TimeUnit {
	/**
	 * Time unit representing one thousandth of a microsecond.
	 */
	NANOSECONDS(),
	/**
	 * Time unit representing one thousandth of a millisecond.
	 */
	MICROSECONDS(),
	/**
	 * Time unit representing one thousandth of a second.
	 */
	MILLISECONDS(),
	/**
	 * Time unit representing one second.
	 */
	SECONDS(),
	/**
	 * Time unit representing sixty seconds.
	 * 
	 * @since 1.6
	 */
	MINUTES(),
	/**
	 * Time unit representing sixty minutes.
	 * 
	 * @since 1.6
	 */
	HOURS(),
	/**
	 * Time unit representing twenty four hours.
	 * 
	 * @since 1.6
	 */
	DAYS();
}
