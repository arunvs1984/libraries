package com.reds.library.redscommons;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

public class RedsUtils {
	
	public static Charset convert(CharSet charSet) {
		switch (charSet) {
		case ISO_8859_1:
			return java.nio.charset.StandardCharsets.ISO_8859_1;
		case US_ASCII:
			return java.nio.charset.StandardCharsets.US_ASCII;
		case UTF_16:
			return java.nio.charset.StandardCharsets.UTF_16;
		case UTF_16BE:
			return java.nio.charset.StandardCharsets.UTF_16BE;
		case UTF_16LE:
			return java.nio.charset.StandardCharsets.UTF_16LE;
		case UTF_8:
			return java.nio.charset.StandardCharsets.UTF_8;
		default:
			break;
		
		}
		return null;
	}

	public static TimeUnit convert(com.reds.library.redscommons.TimeUnit timeUnit) {
		switch (timeUnit) {
		case DAYS:
			return TimeUnit.DAYS;
		case HOURS:
			return TimeUnit.HOURS;
		case MICROSECONDS:
			return TimeUnit.MICROSECONDS;
		case MILLISECONDS:
			return TimeUnit.MILLISECONDS;
		case MINUTES:
			return TimeUnit.MINUTES;
		case NANOSECONDS:
			return TimeUnit.NANOSECONDS;
		case SECONDS:
			return TimeUnit.SECONDS;
		default:
			return TimeUnit.MINUTES;

		}
	}

	public static byte[] serialize(Object obj) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os;
		try {
			os = new ObjectOutputStream(out);
			os.writeObject(obj);
			return out.toByteArray();
		} catch (IOException e) {
			throw new Exception("Failed to serialize  ", e);
		}

	}

	public static Object deserialize(byte[] data) throws Exception {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream is;
		try {
			is = new ObjectInputStream(in);
			return is.readObject();
		} catch (IOException | ClassNotFoundException e) {
			throw new Exception("Failed to deserialize  ", e);
		}

	}
}
