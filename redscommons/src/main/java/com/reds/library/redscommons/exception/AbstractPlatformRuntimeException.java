package com.reds.library.redscommons.exception;

public class AbstractPlatformRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;

	public AbstractPlatformRuntimeException(String code) {
		this.code = code;
	}

	/**
	 * @param code    - Error Code
	 * @param message
	 */
	public AbstractPlatformRuntimeException(String code, String message) {
		super(message);
		this.code = code;

	}

	/**
	 * @param code    - Error Code
	 * @param message
	 * @param cause
	 */
	public AbstractPlatformRuntimeException(String code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;

	}

	/**
	 * @param code               - Error Code
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AbstractPlatformRuntimeException(String code, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.code = code;

	}

}
