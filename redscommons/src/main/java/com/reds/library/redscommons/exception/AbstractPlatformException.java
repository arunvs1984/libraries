package com.reds.library.redscommons.exception;

public abstract class AbstractPlatformException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;

	public AbstractPlatformException(String code) {
		this.code = code;
	}

	
	/**
	 * @param code - Error Code
	 * @param message
	 */
	public AbstractPlatformException(String code, String message) {
		super(message);
		this.code = code;

	}

	/**
	 * @param code  - Error Code
	 * @param message
	 * @param cause
	 */
	public AbstractPlatformException(String code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;

	}

	/**
	 * @param code  - Error Code
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AbstractPlatformException(String code, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.code = code;

	}

}
