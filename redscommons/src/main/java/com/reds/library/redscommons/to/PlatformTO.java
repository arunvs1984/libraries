package com.reds.library.redscommons.to;

import com.reds.library.redscommons.RedsUtils;

public interface PlatformTO {

	public String getId();

	default public byte[] toBytes() throws Exception {
		return RedsUtils.serialize(this);
	}

	default public PlatformTO fromBytes(byte[] toAsBytes) throws Exception {
		return (PlatformTO) RedsUtils.deserialize(toAsBytes);
	}

}
