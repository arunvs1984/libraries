package com.reds.library.redscommons.info;

public interface Info {
	
	public String getName();

	public String getAuthor();

	public String getGeneratedDate();

}
