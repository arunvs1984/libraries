package com.reds.library.redscommons.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.exception.RedscommonsErrorCodes;
import com.reds.library.redscommons.to.PlatformTO;

/**
 * <b>Purpose:</b> Default implementation for {@link RedsXmlManager}
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 * @param <T>
 */
public class RedsXmlManagerImpl<T extends PlatformTO> implements RedsXmlManager<T> {

	private Serializer serializer = new Persister();
	private final String EXTENSION = ".xml";
	private String location = "./xmlstore/";

	/**
	 * @param location
	 */
	public RedsXmlManagerImpl(String location) {
		super();
		this.location = location;
	}

	/* (non-Javadoc)
	 * @see com.reds.library.redscommons.xml.RedsXmlManager#write(com.reds.library.redscommons.dataobject.RedsDO)
	 */
	@Override
	public void write(T redsDo) throws RedsXmlException {
		try {
			File file = new File(location);
			if (!file.exists()) {
				file.mkdirs();
			}
			String location = getLocation(redsDo);
			file = new File(location);
			serializer.write(redsDo, file);

		} catch (Exception cause) {
			throw new RedsXmlException(RedscommonsErrorCodes.errorCode(1), "Failed to write to xml : " + location,
					cause);
		}

	}

	private String getLocation(T xmlObject) {
		return location + File.separator + xmlObject.getId() + EXTENSION;
	}
	private String getLocation() {
		return location + File.separator ;
	}

	/* (non-Javadoc)
	 * @see com.reds.library.redscommons.xml.RedsXmlManager#read(com.reds.library.redscommons.dataobject.RedsDO, java.lang.Class)
	 */
	@Override
	public T read(T redsDo, Class<?> clazz) throws RedsXmlException {
		String location = getLocation(redsDo);
		try {
			return (T) serializer.read(clazz, new File(location));
		} catch (Exception cause) {
			throw new RedsXmlException(RedscommonsErrorCodes.errorCode(1),
					"Failed to read xml from location : " + location, cause);

		}
	}
	
	@Override
	public T read(String location, Class<?> clazz) throws RedsXmlException {
		try {
			return (T) serializer.read(clazz, new File(location));
		} catch (Exception cause) {
			throw new RedsXmlException(RedscommonsErrorCodes.errorCode(1),
					"Failed to read xml from location : " + location, cause);

		}
	}

	/* (non-Javadoc)
	 * @see com.reds.library.redscommons.xml.RedsXmlManager#delete(com.reds.library.redscommons.dataobject.RedsDO)
	 */
	@Override
	public void delete(T redsDo) throws RedsXmlException {
		String location = getLocation(redsDo);
		File file = new File(location);
		file.delete();
	}

	/* (non-Javadoc)
	 * @see com.reds.library.redscommons.xml.RedsXmlManager#deleteAll(com.reds.library.redscommons.dataobject.RedsDO)
	 */
	@Override
	public void deleteAll() throws RedsXmlException {
		List<File> listOfFiles = getAllXmlFiles();
		if (listOfFiles != null) {
			for (File file : listOfFiles) {
				file.delete();
			}
		}
	}

	/**
	 * @param redsDo
	 * @return List of {@link File}
	 */
	private List<File> getAllXmlFiles() {
		String location = getLocation();
		File source = new File(location);
		List<File> files =null;
		if(source.exists()&&source.isDirectory()) {
			 files=(List<File>) FileUtils.listFiles(source, FileFilterUtils.suffixFileFilter(EXTENSION),
					TrueFileFilter.INSTANCE);	
		}
		
		return files;
	}

	/* (non-Javadoc)
	 * @see com.reds.library.redscommons.xml.RedsXmlManager#readAll(com.reds.library.redscommons.dataobject.RedsDO, java.lang.Class)
	 */
	@Override
	public List<T> readAll(Class<?> clazz) throws RedsXmlException {
		List<File> allFiles = getAllXmlFiles();
		List<T> allObjects = new ArrayList<T>();
		if (allFiles != null) {
			for (File file : allFiles) {
				try {
					allObjects.add((T) serializer.read(clazz, file));
				} catch (Exception cause) {
					throw new RedsXmlException(RedscommonsErrorCodes.errorCode(1),
							"Failed to read all xml from location " + file.getAbsolutePath(), cause);

				}
			}
		}

		return allObjects;
	}

	/* (non-Javadoc)
	 * @see com.reds.library.redscommons.xml.RedsXmlManager#exist(com.reds.library.redscommons.dataobject.RedsDO)
	 */
	@Override
	public boolean exist(T redsDo) throws RedsXmlException {
		String location = getLocation(redsDo);
		File file = new File(location);
		return file.exists();
	}

	

}
