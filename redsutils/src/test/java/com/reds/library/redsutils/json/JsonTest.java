package com.reds.library.redsutils.json;

import org.testng.annotations.Test;

import com.reds.library.redsutils.info.RedsutilsInfo;

public class JsonTest {

  @Test
  public void fromJson() {
	  RedsutilsInfo info = new RedsutilsInfo();
    System.out.println(Json.toJson(info));
  }

  @Test
  public void toJson() {
	   String text="{\"name\":\"RedsUtils\",\"author\":\"SHINEED BASHEER\",\"generatedDate\":\"05-11-2018 10:12:04\"}";
	  
	   RedsutilsInfo info = Json.fromJson(text, RedsutilsInfo.class);
	   System.out.println(info);
  }
}
