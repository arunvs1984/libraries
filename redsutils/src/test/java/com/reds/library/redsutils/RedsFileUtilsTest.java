package com.reds.library.redsutils;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.testng.annotations.Test;

import com.reds.library.redsutils.exception.RedsutilsException;

public class RedsFileUtilsTest {

 
  public void recursivelyGetParent() throws URISyntaxException {
	  Path child = Paths.get(RedsFileUtilsTest.class.getResource(".").toURI());
	  System.out.println(RedsFileUtils.recursivelyGetParent(child, "redsutils"));
  }
  
  
  public void getAllFiles() throws RedsutilsException {
	 List<Path> paths= RedsFileUtils.getAllFiles("./lib/");
	 for(Path path:paths) {
		 System.out.println(path.toString());
	 }
  }
  @Test
  public void moveFiles() throws RedsutilsException {
		 RedsFileUtils.moveFiles("./lib/", "./test/");
	  }
}
