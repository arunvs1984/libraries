package com.reds.library.redsutils.json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.reds.library.redsutils.exception.RedsutilsErrorCodes;
import com.reds.library.redsutils.exception.RedsutilsException;

public class Json {
	private static Gson gson = new Gson();

	public static <T> T fromJson(Type type, JsonDeserializer deserializer, String json) {
		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(type, deserializer);
		final Gson gson = gsonBuilder.create();
		return gson.fromJson(json, type);
	}

	public static String toJson(Object input) {
		return gson.toJson(input);
	}

	public static <T> T fromJson(String json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}

	public static <T> T fromJsonFile(String filename,Class<T> classOfT) throws RedsutilsException {
		Gson gson = new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(filename));
			return gson.fromJson(reader, classOfT);
		} catch (FileNotFoundException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to read Json Object from "+filename, e);
		}

	}

	public static void toJsonFile(String filename,Object input) throws RedsutilsException {
		try (FileWriter file = new FileWriter(filename)) {				
			file.write(Json.toJson(input));
			System.out.println("Successfully Copied JSON Object to File...");
		
		} catch (IOException e) {
				throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to write Json Object to file "+ filename, e);
		}
	}

}
