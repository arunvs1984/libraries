package com.reds.library.redsutils.validation;

public class Violations {
	private Class beanClass;
	
	private String message;
	public Violations(Class beanClass, String message) {
		super();
		this.beanClass=beanClass;
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return beanClass.getName()+">"+message;
	}

	public Class getBeanClass() {
		return beanClass;
	}

	public void setBeanClass(Class beanClass) {
		this.beanClass = beanClass;
	}

}
