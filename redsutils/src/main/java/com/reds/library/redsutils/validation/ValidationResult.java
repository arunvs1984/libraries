package com.reds.library.redsutils.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
	private boolean valid = true;
	private List<Violations> violations = null;

	public ValidationResult(boolean valid) {
		this.valid = valid;
		this.violations = new ArrayList<Violations>();
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return "ValidationResult [valid=" + valid + ", violations=" + violations + "]";
	}

	public void add(Violations validationInfo) {
		this.violations.add(validationInfo);

	}

	public List<Violations> getViolations() {
		return violations;
	}

}
