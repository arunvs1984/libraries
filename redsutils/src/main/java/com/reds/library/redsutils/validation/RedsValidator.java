package com.reds.library.redsutils.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.reds.library.redsutils.exception.RedsutilsErrorCodes;
import com.reds.library.redsutils.exception.ValidationException;

public class RedsValidator {

	private static Validator validator;

	static {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();

	}

	public static boolean isValid(Object object) throws ValidationException {
		ValidationResult result = validate(object);
		if (result.isValid()) {
			return true;
		}
		throw new ValidationException(RedsutilsErrorCodes.errorCode(2),"Validation Exception " + result);
	}

	public static ValidationResult validate(Object object) {
		Set<ConstraintViolation<Object>> constraintViolations = RedsValidator.validator.validate(object);
		ValidationResult validationResult = new ValidationResult(constraintViolations.size() > 0 ? false : true);
		for (ConstraintViolation<Object> violations : constraintViolations) {
			validationResult.add(new Violations(violations.getLeafBean().getClass(), violations.getMessage()));

		}
		return validationResult;
	}

}
