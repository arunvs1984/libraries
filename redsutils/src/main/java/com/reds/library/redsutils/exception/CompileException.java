package com.reds.library.redsutils.exception;

public class CompileException extends RedsutilsException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String className;
	private String lineNumber;
	private String message;

	public CompileException(String code,String message, Throwable throwable) {
		super(code,message,throwable);
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
