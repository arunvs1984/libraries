package com.reds.library.redsutils;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.jfrog.build.api.util.ZipUtils;

import com.reds.library.redsutils.exception.RedsutilsErrorCodes;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.library.redsutils.tracker.RedsutilsTracker;

/**
 * <b>Purpose:</b>Utils class for all file related operations
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class RedsFileUtils {

	public static final int BUFFER_SIZE = 4096;

	public static byte[] serialize(Object obj) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os;
		try {
			os = new ObjectOutputStream(out);
			os.writeObject(obj);
			return out.toByteArray();
		} catch (IOException e) {
			throw new Exception("Failed to serialize  ", e);
		}

	}

	public static Object deserialize(byte[] data) throws Exception {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream is;
		try {
			is = new ObjectInputStream(in);
			return is.readObject();
		} catch (IOException | ClassNotFoundException e) {
			throw new Exception("Failed to deserialize  ", e);
		}

	}

	public static byte[] extractBytesFromImage(String ImageName) throws RedsutilsException {

		File imgPath = new File(ImageName);
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(imgPath);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to read image ", e);
		}

		WritableRaster raster = bufferedImage.getRaster();
		DataBufferByte data = (DataBufferByte) raster.getDataBuffer();

		return (data.getData());
	}

	/**
	 * <b>Purpose:</b> Return all the files from the given folder
	 * 
	 * @param dirPath- Source Directory
	 * @return List of all File Paths in the folder
	 * @throws RedsutilsException
	 */
	public static List<Path> getAllFiles(String dirPath) throws RedsutilsException {
		List<Path> allFiles = new ArrayList<>();
		try {
			Files.list(new File(dirPath).toPath()).forEach(e -> {
				allFiles.add(e);
			});
			;
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1),
					"Failed to get all files from given path " + dirPath, e);
		}
		return allFiles;
	}

	public static String readFileFully(Path path) throws RedsutilsException {
		String text;
		try {
			text = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1),
					"Failed to read file completly from given path " + path, e);
		}
		return text;
	}

	public static List<Path> getAllFiles(String dirPath, String filterName) throws RedsutilsException {
		List<Path> allFiles = new ArrayList<>();
		try {
			Files.walk(Paths.get(dirPath)).filter(Files::isRegularFile).forEach(e -> {
				if (e.getFileName().toString().equals(filterName)) {
					allFiles.add(e);
				}

			});

		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1),
					"Failed to get all files from given path " + dirPath, e);
		}
		return allFiles;
	}

	/**
	 * <b>Purpose:</b> Move all files from given folder to destination folder.
	 * 
	 * @param sourcePath
	 * @param destinationPath
	 * @throws RedsutilsException
	 */
	public static void moveFiles(String sourcePath, String destinationPath) throws RedsutilsException {
		try {
			List<Path> allFiles = getAllFiles(sourcePath);
			for (Path path : allFiles) {
				String newFilePah = destinationPath + File.separator + path.getFileName();
				RedsutilsTracker.me.debug("Moving {} to {}", path.toString(), newFilePah);
				if (!new File(newFilePah).exists()) {
					FileUtils.moveFileToDirectory(path.toFile(), new File(destinationPath), false);
				}

			}
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1),
					"Failed to move all files from given path to destination ", e);
		}
	}

	/**
	 * Writes a String to a file creating the file if it does not exist.
	 *
	 * @param location the file to write
	 * @param content  the content to write to the file append if {@code true}, then
	 *                 the String will be added to the end of the file rather than
	 *                 overwriting
	 * @throws RedsutilsException in case of an I/O error
	 * 
	 */
	public static void write(String location, String content) throws RedsutilsException {
		try {
			FileUtils.writeStringToFile(new File(location), content, false);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to create write to file " + location,
					e);
		}
	}

	/**
	 * Writes a String to a file creating the file if it does not exist.
	 *
	 * @param location the file to write
	 * @param content  the content to write to the file
	 * 
	 * @throws RedsutilsException in case of an I/O error
	 * 
	 */
	public static void write(String location, String content, boolean append) throws RedsutilsException {
		try {
			FileUtils.writeStringToFile(new File(location), content, append);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to create write to file " + location,
					e);
		}
	}

	/**
	 * Makes a directory, including any necessary but nonexistent parent
	 * directories. If a file already exists with specified name but it is not a
	 * directory then an IOException is thrown. If the directory cannot be created
	 * (or does not already exist) then an IOException is thrown.
	 *
	 * @param directory directory to create, must not be {@code null}
	 * @throws NullPointerException if the directory is {@code null}
	 * @throws IOException          if the directory cannot be created or the file
	 *                              already exists but is not a directory
	 */
	public static void makeDirForcefuly(String dirPath) throws RedsutilsException {
		try {
			FileUtils.forceMkdir(new File(dirPath));
		} catch (IOException exception) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to create directory.", exception);
		}
	}

	/**
	 * <b>Purpose:</b> To Make Directory
	 * 
	 * @param dirPath
	 * @throws RedsutilsException
	 */
	public static void makeDir(String dirPath) throws RedsutilsException {
		Path dirPathObj = Paths.get(dirPath);
		boolean dirExists = Files.exists(dirPathObj, new LinkOption[] { LinkOption.NOFOLLOW_LINKS });
		if (dirExists) {
			RedsutilsTracker.me.info("Destination directory already exist.", dirPath);
		} else {
			try {
				Files.createDirectories(dirPathObj);
				RedsutilsTracker.me.debug("New Directory Successfully Created ", dirPath);
			} catch (IOException exception) {
				throw new RedsutilsException(RedsutilsErrorCodes.errorCode(2), "Failed to create directory", exception);
			}
		}
	}

	/**
	 * Recursively gets parent of a child directory
	 * 
	 * @param child
	 * @param parentName
	 * @return Path to parent
	 */
	public static Path recursivelyGetParent(Path child, String parentName) {
		if (child == null) {
			return null;
		}
		if (child.getParent() == null) {
			return null;
		}
		if (child.getParent().getFileName() == null) {
			return null;
		}
		String dirName = child.getParent().getFileName().toString();

		if (dirName.equals(parentName)) {
			return child.getParent();
		} else {
			return recursivelyGetParent(child.getParent(), parentName);

		}

	}

	public static void replaceFileContent(String pathString, String stringToReplace, String replaceWith)
			throws RedsutilsException {
		try {
			Path path = Paths.get(pathString);
			Stream<String> lines = Files.lines(path);
			List<String> replaced = lines.map(line -> line.replaceAll(stringToReplace, replaceWith))
					.collect(Collectors.toList());
			Files.write(path, replaced);
			lines.close();

		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(2), "Failed to replace content of file ", e);
		}
	}

	public static void replaceFilesLastContent(String pathString, String replaceWith) throws RedsutilsException {
		try {
			Path path = Paths.get(pathString);
			List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
			lines.set(lines.size() - 1, replaceWith);
			Files.write(path, lines, StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(2), "Failed to replace content of file ", e);
		}
	}

	public static void copyDir(String source, String destinations) throws RedsutilsException {

		try {
			FileUtils.copyDirectory(new File(source), new File(destinations));
			RedsutilsTracker.me.debug("Copied Directory from  {} to destination {}", source, destinations);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(2), "Failed to copy file ", e);
		}

	}

	public static void copy(String source, String destinations) throws RedsutilsException {

		try {
			FileUtils.copyFile(new File(source), new File(destinations));
			RedsutilsTracker.me.debug("Copied File {} to destination {}", source, destinations);
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(2), "Failed to copy file ", e);
		}

	}

	public static void unZip(String sourceArchive, String destinationDirectory) throws RedsutilsException {
		File sourceFile = new File(sourceArchive);
		File destination = new File(destinationDirectory);
		try {
			ZipUtils.extract(sourceFile, destination);

		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1),
					"Failed to unzip the file " + sourceArchive + " to " + destinationDirectory, e);
		}
	}

	public static String readBanner(final Class<?> loadingClass, String resourceName) {
		return readBanner(new InputStreamReader(getInputStream(loadingClass, resourceName)));
	}

	private static String readBanner(Reader reader) {
		try {
			String content = copyToString(new BufferedReader(reader));
			return content.replaceAll("(\\r|\\n)+", OsUtils.LINE_SEPARATOR);
		} catch (Exception ex) {
			throw new IllegalStateException("Cannot read stream", ex);
		}
	}

	/**
	 * Loads the given file from the classpath.
	 *
	 * @param loadingClass the class from whose package to load the file (required)
	 * @param filename     the name of the file to load, relative to that package
	 *                     (required)
	 * @return the file's input stream (never <code>null</code>)
	 * @throws IllegalArgumentException if the given file cannot be found
	 */
	public static InputStream getInputStream(final Class<?> loadingClass, final String filename) {
		final InputStream inputStream = loadingClass.getResourceAsStream(filename);
		return inputStream;
	}

	/**
	 * Copy the contents of the given Reader into a String. Closes the reader when
	 * done.
	 * 
	 * @param in the reader to copy from
	 * @return the String that has been copied to
	 * @throws IOException in case of I/O errors
	 */
	public static String copyToString(Reader in) throws IOException {
		StringWriter out = new StringWriter();
		copy(in, out);
		return out.toString();
	}

	public static int copy(Reader in, Writer out) throws IOException {
		try {
			int byteCount = 0;
			char[] buffer = new char[BUFFER_SIZE];
			int bytesRead = -1;
			while ((bytesRead = in.read(buffer)) != -1) {
				out.write(buffer, 0, bytesRead);
				byteCount += bytesRead;
			}
			out.flush();
			return byteCount;
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
			}
			try {
				out.close();
			} catch (IOException ex) {
			}
		}
	}

	/**
	 * Copy the contents of the given input File to the given output File.
	 * 
	 * @param in  the file to copy from
	 * @param out the file to copy to
	 * @return the number of bytes copied
	 * @throws IOException in case of I/O errors
	 */
	public static int copy(File in, File out) throws IOException {
		return copy(new BufferedInputStream(new FileInputStream(in)),
				new BufferedOutputStream(new FileOutputStream(out)));
	}

	/**
	 * Copy the contents of the given InputStream to the given OutputStream. Closes
	 * both streams when done.
	 * 
	 * @param in  the stream to copy from
	 * @param out the stream to copy to
	 * @return the number of bytes copied
	 * @throws IOException in case of I/O errors
	 */
	public static int copy(InputStream in, OutputStream out) throws IOException {
		try {
			return copyStream(in, out);
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
			}
			try {
				out.close();
			} catch (IOException ex) {
			}
		}
	}

	/**
	 * Copy the contents of the given InputStream to the given OutputStream. Leaves
	 * both streams open when done.
	 * 
	 * @param in  the InputStream to copy from
	 * @param out the OutputStream to copy to
	 * @return the number of bytes copied
	 * @throws IOException in case of I/O errors
	 */
	public static int copyStream(InputStream in, OutputStream out) throws IOException {
		int byteCount = 0;
		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
			byteCount += bytesRead;
		}
		out.flush();
		return byteCount;
	}

	public static Path scanAndGetFilePath(String root, String fileName) throws RedsutilsException {
		try {
			Path start = Paths.get(root);
			Stream<Path> stream = Files.walk(start, FileVisitOption.FOLLOW_LINKS);
			Optional<Path> path = stream.filter(e -> e.getFileName().endsWith(fileName)).findFirst();
			if (path != null) {
				return path.get();
			}

		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to get file", e);
		}
		return null;

	}

	/**
	 * Deletes a file, never throwing an exception. If file is a directory, delete
	 * it and all sub-directories.
	 * <p>
	 * The difference between File.delete() and this method are:
	 * <ul>
	 * <li>A directory to be deleted does not have to be empty.</li>
	 * <li>No exceptions are thrown when a file or directory cannot be deleted.</li>
	 * </ul>
	 *
	 * @param file file or directory to delete, can be <code>null</code>
	 * @return <code>true</code> if the file or directory was deleted, otherwise
	 *         <code>false</code>
	 *
	 * 
	 */
	public static void deleteQuietly(String file) {
		FileUtils.deleteQuietly(new File(file));

	}

	public static void forceDelete(String file) throws RedsutilsException {
		try {
//			Path pathToBeDeleted = new File(file).toPath();
//			Files.walk(pathToBeDeleted)
//		      .sorted(Comparator.reverseOrder())
//		      .map(Path::toFile)
//		      .forEach(e->{
//		    	  try {
//					FileDeleteStrategy.FORCE.delete(e);
//				} catch (IOException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}	  
//		      });
//			
			deleteNew(file);

		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to delete file" + file, e);
		}

	}

	private static void deleteNew(String directoryName) throws IOException {

		Path directory = Paths.get(directoryName);
		Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	public static void delete(String file) throws RedsutilsException {
		try {
			FileUtils.deleteDirectory(new File(file));
		} catch (IOException e) {
			throw new RedsutilsException(RedsutilsErrorCodes.errorCode(1), "Failed to delete file" + file, e);
		}

	}

	public static void rename(String serviceTempRoot, String newName) throws RedsutilsException {
		makeDirForcefuly(newName);
		copyDir(serviceTempRoot, newName);

	}

	public static boolean renameFile(String oldName, String newName) throws RedsutilsException {
		File f1 = new File(oldName);
		File f2 = new File(newName);
		boolean b = f1.renameTo(f2);
		return b;
	}

}
