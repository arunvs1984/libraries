package com.reds.library.redsutils;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

public class RedsStringUtils {

	public static String toCamelCase(String value, boolean startWithLowerCase) {
		String[] strings = StringUtils.split(value.toLowerCase(), "_");
		for (int i = startWithLowerCase ? 1 : 0; i < strings.length; i++) {
			strings[i] = StringUtils.capitalize(strings[i]);
		}
		return StringUtils.join(strings);
	}

	public static String capitalize(String input) {
		return WordUtils.capitalize(input);
	}

	public static String removeSpaces(String data) {
		if (data == null) {
			return data;
		}
		return data.replaceAll("\\s+", "");
	}

	public static String removeLastComma(String value) {
		if (value != null) {
			if (value.lastIndexOf(',') > 0) {
				value = value.substring(0, value.lastIndexOf(','));
			}
		}

		return value;
	}

	public static String toLower(String name) {

		return name.toLowerCase();
	}

	public static String getSimpleName(String fullyQualifiedName) {

		return fullyQualifiedName.substring(fullyQualifiedName.lastIndexOf('.') + 1, fullyQualifiedName.length());
	}

	public static void main(String[] args) {
		System.out.println(splitCamelCaseStringAsString("saveCompany"));
	}

	public static String removeDotByUnderScore(String string) {

		return string.replaceAll("\\.", "_");
	}

	public static List<String> splitCamelCaseString(String s) {
		LinkedList<String> result = new LinkedList<String>();
		for (String w : s.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
			result.add(w);
		}
		return result;
	}
	
	public static String splitCamelCaseStringAsString(String s) {
		StringBuilder result=new StringBuilder();
		for (String w : s.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")) {
			result.append(capitalize(w)).append(" ");
		}
		return result.toString().trim();
	}
	
	
}
