package com.reds.library.redsutils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * <b>Purpose:</>Platform utility class for all date related functions
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class RedsDateUtils {
	/**
	 * To get Current System Date
	 * 
	 * @return Date
	 */

	public static final long WEEKCONSTANT = 7 * 24 * 60 * 60 * 1000;

	public static final long DAYCONSTANT = 24 * 60 * 60 * 1000;

	public static final long MINSCONSTANT = 60 * 1000;

	public static Date getNow() {
		return new Date(System.currentTimeMillis());
	}

	public static String getCurrenTimeStamp() {
		SimpleDateFormat dateFormate = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
		return dateFormate.format(System.currentTimeMillis());
	}

	public static String getCurrenTimeStamp(String formatDate) {
		SimpleDateFormat dateFormate = new SimpleDateFormat(formatDate);
		return dateFormate.format(System.currentTimeMillis());
	}

	public static Date getDate(String stringDate, String formatDate) {

		DateFormat formatter = new SimpleDateFormat(formatDate);
		Date date = null;
		try {
			date = formatter.parse(stringDate);
		} catch (ParseException e) {

		}
		return date;

	}

	public static String getFormatedDate(Date stringDate, String formatDate) {
		DateFormat formatter = new SimpleDateFormat(formatDate);
		String date = null;
		date = formatter.format(stringDate);
		return date;

	}

	public static java.sql.Date convertUtilDateToSqlDate(long timeValue) {
		java.sql.Date sqlDate = null;
		if (timeValue != 0) {
			sqlDate = new java.sql.Date(timeValue);
			return sqlDate;
		}
		return sqlDate;
	}

	public static Date longToDate(long time) {
		Date date = new Date(time);
		return date;
	}

	public static Date timeStampToDate(Timestamp timestamp) {
		Date date = new Date(timestamp.getTime());
		return date;
	}

	public static long convertDateStringToLong(String dateString, String inputFormat) {
		// format Example: yyyyMMdd-hh:mm:ss.SSS
		String string_date = dateString;
		long milliseconds = 0L;
		SimpleDateFormat formatter = new SimpleDateFormat(inputFormat);
		Date d;
		try {
			d = formatter.parse(string_date);
			milliseconds = d.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return milliseconds;
	}

	public static LocalDate convertDateToLocalDate(Date date) {
		Instant instant = date.toInstant();
		LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate;
	}

	public static Date convertLocalDateToDate(LocalDate localDate) {
		Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
		Date date = Date.from(instant);
		return date;
	}

	public static Date addMinutesToDate(int minutes, Date beforeTime) {
		final long ONE_MINUTE_IN_MILLIS = 60000;
		long curTimeInMs = beforeTime.getTime();
		Date afterAddingMins = new Date(curTimeInMs + (minutes * ONE_MINUTE_IN_MILLIS));
		return afterAddingMins;
	}

	public static Date minusMinutesToDate(int minutes, Date beforeTime) {
		final long ONE_MINUTE_IN_MILLIS = 60000;
		long curTimeInMs = beforeTime.getTime();
		Date afterAddingMins = new Date(curTimeInMs - (minutes * ONE_MINUTE_IN_MILLIS));
		return afterAddingMins;
	}

	public static Date getSpecifiedTimeOnDay(Date date) {
		Calendar currentcal = Calendar.getInstance();
		currentcal.setTimeInMillis(System.currentTimeMillis());
		Calendar prevCal = Calendar.getInstance();
		prevCal.setTime(date);
		currentcal.set(Calendar.HOUR_OF_DAY, prevCal.get(Calendar.HOUR_OF_DAY));
		currentcal.set(Calendar.MINUTE, prevCal.get(Calendar.MINUTE));
		currentcal.set(Calendar.SECOND, prevCal.get(Calendar.SECOND));
		currentcal.set(Calendar.MILLISECOND, prevCal.get(Calendar.MILLISECOND));

		return currentcal.getTime();

	}

	public static Date getPreviousDate(Date currentDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		Date previousDate = cal.getTime();
		return previousDate;
	}

	public static Date addMinutesToDate(Date currentDate, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.add(Calendar.MINUTE, minutes);
		Date newDate = cal.getTime();
		return newDate;

	}

	/**
	 * API for getting back Day of the Month
	 * 
	 * @param date
	 * @return
	 */
	public static int getDay(Date date) {

		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		return instance.get(Calendar.DAY_OF_MONTH);

	}

	/**
	 * API for getting back Month of year
	 * 
	 * @param date
	 * @return
	 */
	public static int getMonth(Date date) {

		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		return (instance.get(Calendar.MONTH) + 1);

	}

	/**
	 * API for getting back Month of year
	 * 
	 * @param time
	 * @return
	 */
	public static int getMonth(long time) {

		Calendar instance = Calendar.getInstance();
		instance.setTimeInMillis(time);
		return (instance.get(Calendar.MONTH) + 1);

	}

	/**
	 * API for getting back date of the month
	 * 
	 * @param time
	 * @return
	 */
	public static int getDate(long time) {
		Calendar instance = Calendar.getInstance();
		instance.setTimeInMillis(time);
		return instance.get(Calendar.DATE);
	}

	/**
	 * 
	 * API for getting Month in String
	 * 
	 * @param date
	 * @return
	 */
	public static String getMonthInString(Date date) {

		Calendar instance = Calendar.getInstance();
		instance.setTime(date);

		return Month.of(instance.get(Calendar.MONTH) + 1).toString().substring(0, 3);

	}

	/**
	 * 
	 * API for getting year
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(Date date) {

		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		return instance.get(Calendar.YEAR);

	}

	/**
	 * APi for gettingyear
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(long date) {

		Calendar instance = Calendar.getInstance();
		instance.setTimeInMillis(date);
		return instance.get(Calendar.YEAR);

	}

	/**
	 * API for getting back Seconds from a Date
	 * 
	 * @param date
	 * @return
	 */
	public static int getSecond(Date date) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		return instance.get(Calendar.SECOND);
	}

	/**
	 * API for getting back Minute from a Date
	 * 
	 * @param date
	 * @return
	 */
	public static int getMinute(Date date) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		return instance.get(Calendar.MINUTE);
	}

	/**
	 * API for getting back HOUR from a Date
	 * 
	 * @param date
	 * @return
	 */
	public static int getHour(Date date) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		return instance.get(Calendar.HOUR_OF_DAY);
	}

	public static long rewindWeek(long time, int numWeeks) {
		long fromyear = (time - (numWeeks * WEEKCONSTANT));
		return fromyear;
	}

	public static long rewindDays(long time, int numDays) {
		long fromyear = (time - (numDays * DAYCONSTANT));
		return fromyear;
	}

	/**
	 * API for getting next MIN
	 * 
	 * @param time
	 * @return
	 */
	public static long getNextMinute(long time) {

		return (time + (MINSCONSTANT));
	}

	/**
	 * API for getting next MIN
	 * 
	 * @param time
	 * @return
	 */
	public static long getNextDay(long time) {

		return (time + (DAYCONSTANT));
	}

	/**
	 * API for getting previous week start time
	 * 
	 * By Default gives the previous week Monday 00:00AM
	 * 
	 * 
	 * @return
	 */
	public static long getPreviousWeekStart() {

		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.DATE, -7);
		return c.getTimeInMillis();
	}

	/**
	 * API for getting previous week start time
	 * 
	 * By Default gives the previous week Monday 00:00AM
	 * 
	 * 
	 * @return
	 */
	public static long getPreviousDayStart() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.DATE, -1);
		return c.getTimeInMillis();
	}

	/**
	 * API for getting previous week start time
	 * 
	 * By Default gives the previous week Monday 00:00AM
	 * 
	 * 
	 * @return
	 */
	public static long getPreviousDayEnd() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.DATE, -1);
		return c.getTimeInMillis();
	}

	/**
	 * API for getting previous week end time
	 * 
	 * By Default gives the previous week Friday 11:59 PM
	 * 
	 * @return
	 */
	public static long getPreviousWeekEnd() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.DATE, -2);
		return c.getTimeInMillis();
	}

	/**
	 * API for getting previous month end time
	 * 
	 * By Default gives the previous month start date 00:00 AM
	 * 
	 * @return
	 */
	public static long getPreviousMonthStart() {

		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTimeInMillis();
	}

	/**
	 * API for getting previous month end date 23:59 PM
	 * 
	 * @return
	 */
	public static long getPreviousMonthEnd() {

		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTimeInMillis();
	}

	/**
	 * 
	 * API for getting start of current week 00:00 AM
	 * 
	 * By Default gives the current week Monday 00:00AM
	 * 
	 * @return
	 */
	public static long getCurrentWeekStart() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTimeInMillis();
	}

	/**
	 * 
	 * API adds 24 hours to the date passed and
	 * 
	 * @param time
	 * @return
	 */
	public static boolean isNextMonth(long time) {

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		int current = c.get(Calendar.MONTH);
		c.add(Calendar.DAY_OF_MONTH, 1);
		int next = c.get(Calendar.MONTH);
		if (current == next) {
			return false;
		}
		return true;
	}

	
	/**
	 * 
	 * API gives the current month start
	 * 
	 * By Default gives back the current month 1 00:00AM
	 * 
	 * @return
	 */
	public static long getCurrentMonthStart() {

		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTimeInMillis();
	}

	/**
	 * 
	 * API gives the current day start
	 * 
	 * By Default gives back the current day 00:00AM
	 * 
	 * @return
	 */
	public static long getCurrentDayStart() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTimeInMillis();
	}

	/**
	 * 
	 * API gives the current day start
	 * 
	 * By Default gives back the current day 00:00AM
	 * 
	 * @return
	 */
	public static long getCurrentDayEnd() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTimeInMillis();
	}

	public static long getYesterdayEndTime(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		c.add(Calendar.DATE, -1);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		return c.getTimeInMillis();
	}

	/**
	 * 
	 * API for getting yesterday close time
	 * 
	 * @param time
	 * @return
	 */
	public static long getSameDayEndTime(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		return c.getTimeInMillis();
	}

	/**
	 * 
	 * API for getting the start time of the day
	 * 
	 * Returns 0:00:00 AM of that particular day
	 * 
	 * @param time
	 * @return
	 */
	public static long getDayStartTime(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		return c.getTimeInMillis();

	}

	/**
	 * 
	 * <p>
	 * API for checking whether the next day belongs to the next year on not
	 * </p>
	 * 
	 * @param time
	 * @return
	 */
	public static boolean isNextYear(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		int year = c.get(Calendar.YEAR);
		c.add(Calendar.DATE, 1);
		int newYear = c.get(Calendar.YEAR);
		if (year == newYear) {
			return false;
		}
		return true;
	}

	/**
	 * API for getting previous year start
	 * 
	 * @param time
	 * @return
	 */
	public static long getPreviousYearStart(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		c.add(Calendar.YEAR, -1);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 1);
		return c.getTimeInMillis();
	}

	

	/**
	 * API for checking month change
	 * 
	 * @param time
	 * @return
	 */
	public static boolean isMonthChanged(long time) {
		LocalDate date = Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate current = LocalDate.now();
		if (current.getYear() == date.getYear()) {
			if (current.getMonthValue() != date.getMonthValue()) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * API for checking month change
	 * 
	 * @param time
	 * @return
	 */
	public static boolean isYearChanged(long time) {
		LocalDate date = Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate current = LocalDate.now();
		if (current.getYear() == date.getYear()) {
			return false;
		}
		return true;
	}

	/**
	 * API for getting previous year date for a date.
	 * 
	 * @param time
	 * @return
	 */
	public static long getPreviousMonthDate(long time) {
		LocalDate localDate = Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDate();
		long epochDate = localDate.minusMonths(1).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
		return epochDate;
	}

	/**
	 * API for getting previous year date for a date.
	 * 
	 * @param time
	 * @return
	 */
	public static long getPreviousWeekDate(long time) {
		LocalDate localDate = Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDate();
		long epochDate = localDate.minusWeeks(1).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
		return epochDate;
	}

	/**
	 * API for getting previous year date for a date.
	 * 
	 * @param time
	 * @return
	 */
	public static long minusDays(long time, int days) {
		LocalDate localDate = Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDate();
		long epochDate = localDate.minusDays(days).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
		return epochDate;
	}

	/**
	 * 
	 * API for getting previous year end
	 * 
	 * @param time
	 * @return
	 */
	public static long getPreviousYearEnd(long time) {

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		c.add(Calendar.YEAR, -1);
		c.set(Calendar.MONTH, Calendar.DECEMBER);
		c.set(Calendar.DAY_OF_MONTH, 31);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTimeInMillis();

	}

	/**
	 * 
	 * API for adding days
	 * 
	 * @param time
	 * @param numDays
	 * @return
	 */
	public static long addDay(long time, int numDays) {

		return (time + (numDays * DAYCONSTANT));
	}

	/**
	 * 
	 * API for getting previous year end
	 * 
	 * @param time
	 * @return
	 */
	public static long getCurrentYearStart(long time) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 1);
		return c.getTimeInMillis();

	}

	/**
	 * 
	 * API for checking whether two long values belong to the same day
	 * 
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static boolean isSameDay(long time1, long time2) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time1);
		int year1 = cal.get(Calendar.YEAR);
		int month1 = cal.get(Calendar.MONTH);
		int day1 = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTimeInMillis(time2);
		int year2 = cal.get(Calendar.YEAR);
		int month2 = cal.get(Calendar.MONTH);
		int day2 = cal.get(Calendar.DAY_OF_MONTH);

		if ((year1 == year2) && (month1 == month2) && (day1 == day2)) {

			return true;
		}

		return false;
	}

	/**
	 * To check if Date 1 Less than Equal Date 2
	 * 
	 * @param date1
	 * @param date2
	 * @return true only if Date1 <= Date 2
	 */
	public static boolean isLessThanEqual(Date date1, Date date2) {
		if (date1.compareTo(date2) < 0 || date1.compareTo(date2) == 0) {
			return true;
		} else
			return false;
	}

	/**
	 * To check if Date 1 Less than Date 2
	 * 
	 * @param date1
	 * @param date2
	 * @return true only if Date1 < Date 2
	 */
	public static boolean isLessThan(Date date1, Date date2) {
		if (date1.compareTo(date2) < 0) {
			return true;
		} else
			return false;
	}

	/**
	 * To check if Date 1 Greater than or Equal Date 2
	 * 
	 * @param date1
	 * @param date2
	 * @return true only if Date1 >= Date 2
	 */
	public static boolean isGreaterEqual(Date date1, Date date2) {
		if (date1.compareTo(date2) > 0 || date1.compareTo(date2) == 0) {
			return true;
		} else
			return false;
	}

	/**
	 * To check if Date 1 Greater than Date 2
	 * 
	 * @param date1
	 * @param date2
	 * @return true only if Date1 > Date 2
	 */
	public static boolean isGreater(Date date1, Date date2) {
		if (date1.compareTo(date2) > 0) {
			return true;
		} else
			return false;
	}

	/**
	 * To check if Date 1 Equal than Date 2
	 * 
	 * @param date1
	 * @param date2
	 * @return true only if Date1 = Date 2
	 */
	public static boolean isEqual(Date date1, Date date2) {
		if (date1.compareTo(date2) == 0) {
			return true;
		} else
			return false;
	}

}
