package com.reds.library.redsutils.json;

import java.lang.reflect.Type;

public class JsonDeserializers {
	private Type type;
	private JsonDeserializer deserializer;
	public JsonDeserializers(Type type, JsonDeserializer deserializer) {
		super();
		this.type = type;
		this.deserializer = deserializer;
	}
	public JsonDeserializer getDeserializer() {
		return deserializer;
	}
	public Type getType() {
		return type;
	}

}
