package com.reds.library.redsutils.inject;

import com.reds.library.redsutils.info.RedsutilsInfo;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RedsutilsModule_ProvideRedsutilsInfoFactory implements Factory<RedsutilsInfo> {
  private final RedsutilsModule module;

  public RedsutilsModule_ProvideRedsutilsInfoFactory(RedsutilsModule module) {
    this.module = module;
  }

  @Override
  public RedsutilsInfo get() {
    return provideInstance(module);
  }

  public static RedsutilsInfo provideInstance(RedsutilsModule module) {
    return proxyProvideRedsutilsInfo(module);
  }

  public static RedsutilsModule_ProvideRedsutilsInfoFactory create(RedsutilsModule module) {
    return new RedsutilsModule_ProvideRedsutilsInfoFactory(module);
  }

  public static RedsutilsInfo proxyProvideRedsutilsInfo(RedsutilsModule instance) {
    return Preconditions.checkNotNull(
        instance.provideRedsutilsInfo(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
