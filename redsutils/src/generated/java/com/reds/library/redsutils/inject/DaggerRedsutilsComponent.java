package com.reds.library.redsutils.inject;

import com.reds.library.redsutils.info.RedsutilsInfo;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerRedsutilsComponent implements RedsutilsComponent {
  private RedsutilsModule redsutilsModule;

  private DaggerRedsutilsComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static RedsutilsComponent create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.redsutilsModule = builder.redsutilsModule;
  }

  @Override
  public RedsutilsInfo takeRedsutilsInfo() {
    return RedsutilsModule_ProvideRedsutilsInfoFactory.proxyProvideRedsutilsInfo(redsutilsModule);
  }

  public static final class Builder {
    private RedsutilsModule redsutilsModule;

    private Builder() {}

    public RedsutilsComponent build() {
      if (redsutilsModule == null) {
        this.redsutilsModule = new RedsutilsModule();
      }
      return new DaggerRedsutilsComponent(this);
    }

    public Builder redsutilsModule(RedsutilsModule redsutilsModule) {
      this.redsutilsModule = Preconditions.checkNotNull(redsutilsModule);
      return this;
    }
  }
}
